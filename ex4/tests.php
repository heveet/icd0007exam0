<?php

require_once '../vendor/common.php';

class Ex4Tests extends UnitTestCaseExtended {

    function shouldCalculateStandardDeviationOfStudentGrades() {

        $output = $this->executeAndGetOutput('ex4.php');

        $this->assertPattern("/Alice: 0.47/", $output);

        $this->assertPattern("/Bob: 0/", $output);
    }

    function doNotShowEntriesWithoutGrades() {

        $output = $this->executeAndGetOutput('ex4.php');

        $this->assertNoPattern("/Carol: /", $output);
    }

    private function executeAndGetOutput($scriptFile) {
        ob_start();

        include $scriptFile;

        return ob_get_clean();
    }
}

(new Ex4Tests())->run(new PointsReporter(10,
    [1 => 8,
     2 => 10]));
